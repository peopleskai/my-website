$(document).ready(function () {
  $('#introBg').resize(function() {
    $('#introBg').css('top', $('.navbar').outerHeight())
  })

  $('.fullscreen-sticky-navbar').resize(function () {
    var height = $(window).height() - $('.navbar').outerHeight()
    $('.fullscreen-sticky-navbar').height(height);
  }).resize();
});

$(document).ready(function() {
  $(window).scroll(function() {
    console.log(`scroll event y: ${window.scrollY}`)
    $('#introBg').css('transform', `translate3d(0px, ${window.scrollY * -0.3}px, 0`)
  })
})